# Reespirator

This code is meant to provide an open-source baseline for a medical test.

## Resources

* [Arduino Reference](https://www.arduino.cc/reference/en/)
* [Arduino Best Practices and Gotchas](https://www.theatreofnoise.com/2017/05/arduino-ide-best-practices-and-gotchas.html)
* [C++ Best Practices (Gitbook)](https://lefticus.gitbooks.io/cpp-best-practices/content/)

## Last Version Update (david8128)

The code has been downgraded to just use motors only with the magnetic sensor, removing or leaving in comments any section of Pressure Sensors, Volume Sensors, using touch screens or displays.

Some changes in the defaults.h have been done due to the mechanical assembly, nevertheless everything that should need to be better documented may deserve an issue in the Gitlab Workflow.

daafonsecato@unal.edu.co
Universidad Nacional de Colombia    