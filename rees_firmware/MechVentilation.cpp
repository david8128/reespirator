/** Mechanical ventilation.
 *
 * @file MechVentilation.cpp
 *
 * This is the mechanical ventilation software module.
 * It handles the mechanical ventilation control loop.
 */

#include "MechVentilation.h"

int currentWaitTriggerTime = 0;
int currentStopInsufflationTime = 0;
float currentFlow = 0;

MechVentilation::MechVentilation(
    FlexyStepper *stepper,
    Sensors *sensors,
    AutoPID *pid,
    VentilationOptions_t options)
{

    _init(
        stepper,
        sensors,
        pid,
        options);
}

//TODO: use this method to play a beep in main loop, 1 second long for example.
boolean MechVentilation::getStartWasTriggeredByPatient()
{ //returns true if last respiration cycle was started by patient trigger. It is cleared when read.
    if (_startWasTriggeredByPatient)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//TODO: use this method to play a beep in main loop, 2 second long for example.
boolean MechVentilation::getSensorErrorDetected()
{ //returns true if there was an sensor error detected. It is cleared when read.
    if (_sensor_error_detected)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void MechVentilation::start(void)
{
    _running = true;
}

void MechVentilation::stop(void)
{
    _running = false;
}

uint8_t MechVentilation::getRPM(void)
{
    return _rpm;
}
short MechVentilation::getExsuflationTime(void)
{
    return _timeoutEsp;
}
short MechVentilation::getInsuflationTime(void)
{
    return _timeoutIns;
}

short MechVentilation::getPeakInspiratoryPressure(void)
{
    return _pip;
}

short MechVentilation::getPeakEspiratoryPressure(void)
{
    return _peep;
}

State MechVentilation::getState(void)
{
    return _currentState;
}

float MechVentilation::getSPPos(void)
{
    return _stepperPos;
}

void MechVentilation::setRPM(uint8_t rpm)
{
    _rpm = rpm;
    _ins_speed = rpm * 267 / 10 + 267;
    _esp_speed = rpm * STEPPER_SPEED_EXSUFFLATION / 20;
    _setInspiratoryCycle();
}

void MechVentilation::setPeakInspiratoryPressure(float pip)
{
    _pip = pip;
}

void MechVentilation::setPeakEspiratoryPressure(float peep)
{
    _peep = peep;
}

void MechVentilation::_setInspiratoryCycle(void)
{
    float timeoutCycle = ((float)60) * 1000 / ((float)_rpm); // Tiempo de ciclo en msegundos
    _timeoutIns = timeoutCycle * DEFAULT_INSPIRATORY_FRACTION;
    _timeoutEsp = (timeoutCycle)-_timeoutIns;
}

void MechVentilation::evaluatePressure(void)
{
    if (_currentPressure > ALARM_MAX_PRESSURE)
    {
        digitalWrite(PIN_BUZZ, HIGH);
        _currentAlarm = Alarm_Overpressure;
    }
    else if (_currentPressure < ALARM_MIN_PRESSURE)
    {
        digitalWrite(PIN_BUZZ, HIGH);
        _currentAlarm = Alarm_Underpressure;
    }
    else
    {
        if (_currentAlarm != No_Alarm)
        {
            digitalWrite(PIN_BUZZ, LOW);
            _currentAlarm = No_Alarm;
        }
    }

    // Valve
    if (_currentPressure > VALVE_MAX_PRESSURE)
    {
        digitalWrite(PIN_SOLENOID, SOLENOID_OPEN);
    }
}

void MechVentilation::activateRecruitment(void)
{
    _nominalConfiguration.pip = _pip;
    _nominalConfiguration.timeoutIns = _timeoutIns;
    _pip = DEFAULT_RECRUITMENT_PIP;
    _timeoutIns = DEFAULT_RECRUITMENT_TIMEOUT;
    _recruitmentMode = true;
    setState(Init_Insufflation);
}

void MechVentilation::deactivateRecruitment(void)
{
    _pip = _nominalConfiguration.pip;
    _timeoutIns = _nominalConfiguration.timeoutIns;
    _recruitmentMode = false;
    setState(Init_Exsufflation);
}

/**
 * It's called from timer1Isr
 */
void MechVentilation::update(void)
{

    static int totalCyclesInThisState = 0;
    static int currentTime = 0;
    static int flowSetpoint = 0;

    SensorPressureValues_t pressures = _sensors->getRelativePressureInCmH2O();
    _currentPressure = pressures.pressure1;

    // Check pressures
    evaluatePressure();

    refreshWatchDogTimer();

    switch (_currentState)
    {
    case Init_Insufflation:
    {
        // Close Solenoid Valve
        digitalWrite(PIN_SOLENOID, SOLENOID_CLOSED);

        totalCyclesInThisState = (_timeoutIns) / TIME_BASE;

        /* Stepper control: set acceleration and end-position */
        // _stepper->setSpeedInStepsPerSecond(_ins_speed);
        _stepper->setSpeedInStepsPerSecond(STEPPER_SPEED_INSUFFLATION);
        _stepper->setAccelerationInStepsPerSecondPerSecond(
            STEPPER_ACC_INSUFFLATION);
        _stepper->setTargetPositionInSteps(STEPPER_HIGHEST_POSITION);

        _pid->reset();

        /* Status update, reset timer, for next time, and reset PID integrator to zero */
        setState(State_Insufflation);

        currentTime = 0;
    }
    break;

    /*
     * Insufflation
     */
    case State_Insufflation:
    {

        /* Stepper control: set end position */

        // time expired
        if (currentTime > totalCyclesInThisState)
        {
            setState(Init_Exsufflation);
            currentTime = 0;

            if (_recruitmentMode)
            {
                deactivateRecruitment();
            }
        }
        else
        {
            // _pid->run(_currentPressure, (float)_pip, &_stepperSpeed);

            // _stepper->setSpeedInStepsPerSecond(abs(_ins_speed));
            // _stepper->setSpeedInStepsPerSecond(abs(STEPPER_SPEED_INSUFFLATION));

            _pid->run(_currentPressure, (float)_pip, &_stepperPos);
            _stepper->setTargetPositionInSteps(abs(_stepperPos));

            if (_recruitmentMode)
            {
                if (_currentPressure > DEFAULT_RECRUITMENT_PIP + 2)
                {
                    digitalWrite(PIN_SOLENOID, SOLENOID_OPEN);
                }
                else if (_currentPressure < DEFAULT_RECRUITMENT_PIP - 0.5)
                {
                    digitalWrite(PIN_SOLENOID, SOLENOID_CLOSED);
                }
            }

            currentTime++;
        }
    }
    break;
    case Init_Exsufflation:
    {
        // Open Solenoid Valve
        // digitalWrite(PIN_SOLENOID, SOLENOID_OPEN);

        totalCyclesInThisState = _timeoutEsp / TIME_BASE;

        /* Stepper control*/
        // _stepper->setSpeedInStepsPerSecond(_esp_speed);
        _stepper->setSpeedInStepsPerSecond(STEPPER_SPEED_EXSUFFLATION);
        _stepper->setAccelerationInStepsPerSecondPerSecond(
            STEPPER_ACC_EXSUFFLATION);
        _stepper->setTargetPositionInSteps(
            STEPPER_DIR * (STEPPER_LOWEST_POSITION));

        setState(State_Exsufflation);
        currentTime = 0;
    }
    break;

    /*
     * Exsufflation
     */
    case State_Exsufflation:
    {
        // Time has expired
        if (currentTime > totalCyclesInThisState)
        {
            /* Status update and reset timer, for next time */
            setState(Init_Insufflation);
            _startWasTriggeredByPatient = false;

            currentTime = 0;
        }
        else
        {
            // _pid->run(_currentPressure, (float)_peep, &_stepperSpeed);

            // _stepper->setSpeedInStepsPerSecond(abs(_esp_speed));
            _stepper->setSpeedInStepsPerSecond(abs(STEPPER_SPEED_EXSUFFLATION));

            currentTime++;
        }
    }
    break;

    case State_Homing:
    {
        // Open Solenoid Valve
        digitalWrite(PIN_SOLENOID, SOLENOID_OPEN);

        /*
                 * If not in home, do Homing.
                 * 0: stepper is in home
                 * 1: stepper is not in home
                 */

        if (digitalRead(PIN_STEPPER_ENDSTOP))
        {

            /* Stepper control: homming */
            // #if DEBUG_UPDATE
            //             Serial.println("Attempting homing...");
            // #endif
            if (_stepper->moveToHomeInSteps(
                    STEPPER_HOMING_DIRECTION,
                    STEPPER_HOMING_SPEED,
                    STEPPER_STEPS_PER_REVOLUTION * STEPPER_MICROSTEPS,
                    PIN_STEPPER_ENDSTOP) != true)
            {
                // #if DEBUG_UPDATE
                //                 Serial.println("Homing failed");
                // #endif
            }
            _stepper->setTargetPositionRelativeInSteps(0L);
            _stepper->setCurrentPositionInSteps(0L);
        }

        /* Status update and reset timer, for next time */
        currentTime = 0;
        setState(Init_Insufflation);
    }
    break;

    case State_Error:
        break;
    default:
        //TODO
        break;
    }
}

void MechVentilation::_init(
    FlexyStepper *stepper,
    Sensors *sensors,
    AutoPID *pid,
    VentilationOptions_t options)
{
    /* Set configuration parameters */
    _stepper = stepper;
    _sensors = sensors;
    _pid = pid;
    _rpm = options.respiratoryRate;
    _pip = options.peakInspiratoryPressure;
    _peep = options.peakEspiratoryPressure;
    setRPM(_rpm);
    _hasTrigger = options.hasTrigger;
    if (_hasTrigger)
    {
        _triggerThreshold = options.triggerThreshold;
    }
    else
    {
        _triggerThreshold = FLT_MAX;
    }

    /* Initialize internal state */
    _currentState = State_Homing;
    _stepperSpeed = STEPPER_SPEED_DEFAULT;
    _stepperPos = STEPPER_LOWEST_POSITION;

    //
    // connect and configure the stepper motor to its IO pins
    //
    //;
    _stepper->connectToPins(PIN_STEPPER_STEP, PIN_STEPPER_DIRECTION);
    _stepper->setStepsPerRevolution(STEPPER_STEPS_PER_REVOLUTION * STEPPER_MICROSTEPS);

    _sensor_error_detected = false;
}

void MechVentilation::setState(State state)
{
    _currentState = state;
}

// void MechVentilation::_setAlarm(Alarm alarm)
// {
//     _currentAlarm = alarm;
// }
