/**
 * Sensors reading module
 */
#include "Sensors.h"
#include <Arduino.h>
#include <Wire.h>

unsigned int Sensors::begin(void)
{
    return 0;
}

Sensors::Sensors(void)
{
    _init();
}

void Sensors::_init()
{
}

void Sensors::readPressure()
{
    float pres1, pres2;
    // Acquire sensors data
    pres1 = analogRead(PF7) / 20;
    _pressure1 = pres1;
    if (_minPressure > _pressure1)
    {
        _minPressure = _pressure1;
    }
    if (_maxPressure < _pressure1)
    {
        _maxPressure = _pressure1;
    }
}

/**
 * @brief Get absolute pressure in pascals.
 *
 * @return SensorValues_t - pressure values
 */
SensorPressureValues_t Sensors::getAbsolutePressureInPascals()
{
    SensorPressureValues_t values;
    values.pressure1 = _pressure1;
    values.pressure2 = _pressure2 + _pressureSensorsOffset;
    return values;
}

/**
 * @brief Get relative pressure in pascals.
 *
 * @return SensorValues_t - pressure values
 */
SensorPressureValues_t Sensors::getRelativePressureInPascals()
{
    SensorPressureValues_t values = getAbsolutePressureInPascals();
    values.pressure1 = 0;
    values.pressure2 = values.pressure2 - values.pressure1;
    return values;
}

/**
 * @brief Get absolute pressure in H2O cm.
 *
 * @return SensorValues_t - pressure values
 */
SensorPressureValues_t Sensors::getAbsolutePressureInCmH2O()
{
    SensorPressureValues_t values = getAbsolutePressureInPascals();
    values.pressure1 *= DEFAULT_PA_TO_CM_H2O;
    values.pressure2 *= DEFAULT_PA_TO_CM_H2O;
    return values;
}

SensorLastPressure_t Sensors::getLastPressure(void)
{
    SensorLastPressure_t lastPres;
    lastPres.minPressure = _lastMinPressure;
    lastPres.maxPressure = _lastMaxPressure;
    return lastPres;
}

/**
 * @brief Get relative pressure in H2O cm.
 *
 * @return SensorValues_t - pressure values
 */
SensorPressureValues_t Sensors::getRelativePressureInCmH2O()
{
    SensorPressureValues_t values;
    values.pressure1 = _pressure1;
    return values;
}

/**
 * @brief Get the Offset Between Pressure Sensors object
 *
 * This function must be called when flow is 0.
 *
 * @param sensors - pressure sensors that derive flow
 * @param samples - number of samples to compute offset
 * @return float - averaged offset bewteen pressure readings
 */
void Sensors::getOffsetBetweenPressureSensors(int samples)
{
    SensorPressureValues_t values;
    float deltaPressure, deltaAvg;
    float cumDelta = 0.0;
    for (int i = 0; i < samples; i++)
    {
        readPressure();
        values = getAbsolutePressureInPascals();
        deltaPressure = values.pressure1 - values.pressure2;
        cumDelta += deltaPressure;
    }
    deltaAvg = cumDelta / samples;
    _pressureSensorsOffset = deltaAvg;
}

void Sensors::resetPressures(void)
{
    _minPressure = 255;
    _maxPressure = 0;
}
