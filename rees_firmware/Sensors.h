#ifndef _SENSORS_H_
#define _SENSORS_H_

#include <stdint.h>
#include "defaults.h"
#include <Arduino.h>
#include <Wire.h>

#define SENSORS_MAX_ERRORS 5

#if ENABLED_SENSOR_VOLUME_SFM3300
#define SFM3300_OFFSET 32768
#define SFM3300_SCALE 120
#endif

typedef struct
{
    uint8_t minPressure;
    uint8_t maxPressure;
} SensorLastPressure_t;

typedef struct
{
    float pressure1;
    float pressure2;
} SensorPressureValues_t;

class Sensors
{
public:
    Sensors();
    unsigned int begin(void);
    void readPressure(void);
    SensorLastPressure_t getLastPressure(void);
    SensorPressureValues_t getRelativePressureInPascals(void);
    SensorPressureValues_t getAbsolutePressureInPascals(void);
    SensorPressureValues_t getAbsolutePressureInCmH2O(void);
    SensorPressureValues_t getRelativePressureInCmH2O(void);
    void resetPressures(void);
    void getOffsetBetweenPressureSensors(int samples = 100);

private:
    void _init(void);
    uint8_t _minPressure;
    uint8_t _maxPressure;
    float _pressure1;
    float _pressure2;
    float _pressureSensorsOffset = 0.0;
    volatile uint8_t _lastMinPressure;
    volatile uint8_t _lastMaxPressure;
};

#endif
