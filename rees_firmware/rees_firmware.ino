/**
 * @file rees_firmware.ino
 * @author Reesistencia Team
 * @brief Modified by David Fonseca FOR VENTO 1.0
 * @version 0.2
 * @date 2020-03-29
 * 
 * @copyright GPLv3
 * 
 */

/* FALTA DEJAR FUNCIONAL:
TAREA 1. EL INGRESO DE DATOS POR POTENCIOMETROS Y BOTONES DE PIP Y PEEP
TAREA 2. LA SECUENCIA DE INICIO PARA GARANTIZAR EL BALÓN EXTENDIDO ESTANDO LA LEVA ARRIBA */

/*     MEJORAS PARA VENTO 2.0
FUTURO CONTROL DE VOLUMEN TIDAL DE SER NECESARIO
ALARMAS DE PRESIÓN EN CASO DE VOLUMEN CONTROL */

/**
 * Dependencies
 */

#include "defaults.h"
#include "calc.h"
#include "Sensors.h"
#include "MechVentilation.h"

#include "src/AutoPID/AutoPID.h"
#include "src/FlexyStepper/FlexyStepper.h"
#include "src/TimerOne/TimerOne.h"
#include "src/TimerThree/TimerThree.h"

/**
 * Variables
 */

FlexyStepper *stepper = new FlexyStepper();
Sensors *sensors;
AutoPID *pid;
MechVentilation *ventilation;

VentilationOptions_t options;

/**
 * Setup
 */

void setup()
{
    // Puertos serie
    Serial.begin(9600);

    // Zumbador - falta por conectar
    pinMode(PIN_BUZZ, OUTPUT);
    digitalWrite(PIN_BUZZ, HIGH); // test zumbador
    delay(100);
    digitalWrite(PIN_BUZZ, LOW);

    // FC efecto hall
    pinMode(PIN_STEPPER_ENDSTOP, INPUT_PULLUP); // el sensor de efecto hall da un 1 cuando detecta

    // Sensores de presión
    sensors = new Sensors();
    int check = sensors->begin();

    // PID
    pid = new AutoPID(PID_MIN, PID_MAX, PID_KP, PID_KI, PID_KD);
    // ** BangBang en cmH2O **
    // if pressure is more than PID_BANGBANG below or above setpoint,
    // output will be set to min or max respectively en cmH2O
    pid->setBangBang(PID_BANGBANG);
    // set PID update interval
    pid->setTimeStep(PID_TS);

    // Parte motor
    pinMode(PIN_STEPPER_EN, OUTPUT);
    digitalWrite(PIN_STEPPER_EN, HIGH);

    // TODO: Añadir aquí la configuarcion inicial desde puerto serie

    // options.height = DEFAULT_HEIGHT;                                     // PARA FUTURO CONTROL DE VOLUMEN TIDAL DE SER NECESARIO
    // options.sex = DEFAULT_SEX;                                           // PARA FUTURO CONTROL DE VOLUMEN TIDAL DE SER NECESARIO
    options.respiratoryRate = DEFAULT_RPM;
    options.peakInspiratoryPressure = DEFAULT_PEAK_INSPIRATORY_PRESSURE;
    options.peakEspiratoryPressure = DEFAULT_PEAK_ESPIRATORY_PRESSURE;
    options.triggerThreshold = DEFAULT_TRIGGER_THRESHOLD;
    options.hasTrigger = false;

    ventilation = new MechVentilation(
        stepper,
        sensors,
        pid,
        options);

    // // IMPRIMIR DATOS POR SERIAL PARA VALIDAR DATOS DE TIEMPOS
    //Serial.println("Tiempo del ciclo (seg):" + String(ventilation->getExsuflationTime() + ventilation->getInsuflationTime()));
    //Serial.println("Tiempo inspiratorio (mseg):" + String(ventilation->getInsuflationTime()));
    //Serial.println("Tiempo espiratorio (mseg):" + String(ventilation->getExsuflationTime()));

    // TODO: Esperar aqui a iniciar el arranque desde el serial, FALATA *BOTON INICIO*

    // Habilita el motor
    digitalWrite(PIN_STEPPER_EN, LOW);

    // configura la ventilación
    ventilation->start();  //
    ventilation->update(); //

    delay(1000);

    sensors->readPressure(); //

    //TODO: Option: if (Sensores ok) { arranca timer3 }
    Timer3.initialize(50); //50us
    Timer3.attachInterrupt(timer3Isr);

    // TODO: Make this period dependant of TIME_BASE
    // TIME_BASE * 1000 does not work!!
    // Timer1.initialize(50000); // 50 ms
    Timer1.initialize(20000);
    Timer1.attachInterrupt(timer1Isr);

    ventilation->setPeakInspiratoryPressure(30);
    ventilation->setPeakEspiratoryPressure(10);
    ventilation->setRPM(20);
    // attachInterrupt(digitalPinToInterrupt(PIN_STEPPER_ALARM), driverAlarmIsr, FALLING);  // PARA MEJORAS EN LAS ALARMAS
}

/**
 * Loop
 */

// void loop() // TAREA 1
// {
//     VALOR_POTENCIOMETRO1 = analogRead(PIN_POTENCIOMETRO1);
//     VALOR_POTENCIOMETRO2 = analogRead(PIN_POTENCIOMETRO2);
//     VALOR_POTENCIOMETRO3 = analogRead(PIN_POTENCIOMETRO3);
//     //Serial.println(VALOR_POTENCIOMETRO1 + " " + VALOR_POTENCIOMETRO2 + " " +VALOR_POTENCIOMETRO3);
//     STATE_BOTON1 = digitalRead(PIN_BOTON1);
//     STATE_BOTON2 = digitalRead(PIN_BOTON2);
//     STATE_BOTON3 = digitalRead(PIN_BOTON3);
//     STATE_BOTON4 = digitalRead(PIN_BOTON4);
//     Serial.println(String(STATE_BOTON1) + " " + String(STATE_BOTON2) + " " + String(STATE_BOTON3) + " " + String(STATE_BOTON4));
// }

void loop()
{
    // Serial.println(ventilation->getSPPos());

    unsigned long time;
    time = millis();
    unsigned long static lastReadSensor = 0;
    unsigned long static lastSendConfiguration = 0;
    State static lastState;

    if (time > lastSendConfiguration + TIME_SEND_CONFIGURATION)
    {
    }

    if (time > lastReadSensor + TIME_SENSOR)
    {
        sensors->readPressure();                                                 // Hace lectura del pin del sensor
        SensorPressureValues_t pressure = sensors->getRelativePressureInCmH2O(); // Solo es un getPressure

        // char string[100];
        // sprintf(string, "%05d", ((int)pressure.pressure1));
        // Serial.println(string); // IMPRIME el valor de sensor dividido en 20
        Serial.println(String(pressure.pressure1) + " " + String(ventilation->getSPPos()));

        lastReadSensor = time;

        /*
         * Notify insufflated volume
         */
        if (ventilation->getState() != lastState)
        {
            SensorLastPressure_t lastPressure = sensors->getLastPressure();

            if (ventilation->getState() == Init_Exsufflation)
            {
                // Serial.println("P:" + String(pressure.pressure1) + " M:" + String(lastPressure.maxPressure) + " A:" + String(lastPressure.minPressure));
                sensors->resetPressures();
            }
            else if (ventilation->getState() == State_Exsufflation)
            {
                if (lastState != Init_Exsufflation)
                {
                    // Serial.println("P:" + String(pressure.pressure1) + " M:" + String(lastPressure.maxPressure) + " A:" + String(lastPressure.minPressure));
                    sensors->resetPressures();
                }
            }
        }
        lastState = ventilation->getState();
    }
}

void timer1Isr(void)
{
    ventilation->update();
}

void timer3Isr(void)
{
    stepper->processMovement();
}
